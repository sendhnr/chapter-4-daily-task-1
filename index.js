require('dotenv').config();

const http = require('http');
const fs = require('fs');
const port = 3000;
const data = fs.readFileSync('./data.js');

const identitas = JSON.parse(data);
//deklarasi function untuk membaca file css
function getCSS(cssFileName) {
    const cssFilePath = path.join(aset, cssFileName);
    return fs.readFileSync(cssFilePath, { encoding: "utf8" });
  }
  //assign directory path untuk get css
  const aset = path.join(__dirname, "/public/css/");
  
  //deklarasi function untuk membaca file image
  function getIMG(imgFileName) {
    const gambar = path.join(__dirname, "/public/img/");
    const imgFilePath = path.join(gambar, imgFileName);
    return imgFilePath;
  }

http
    .createServer((req, res) => {
        res.writeHead(200, {
            'Content-Type': 'text/html',
        });
        const url = req.url;
        if(url === '/about') {
            fs.readFile('./public/about.html', (error, data) => {
                if(error) {
                    res.writeHead(404);
                    res.write('Error: File not found');
                } else {
                    res.write(data);
                }
                res.end();
            });
        } else if ( url === '/404') {
            fs.readFile('./public/404.html', (error, data) => {
                if(error) {
                    res.writeHead(404);
                    res.write('Error: File not found');
                } else {
                    res.write(data);
                }
                res.end();
            });
        } else if (url === '/style.css') {
            res.writeHead(200, { "Content-type": "text/css" });
            // res.write is an inbuilt Application program Interface of the ‘http’ module which sends a chunk of the response body that is omitted when the request is a HEAD request. If this method is called and response.writeHead() has not been called, it will switch to implicit header mode and flush the implicit headers
            res.write(getCSS("/style.css"));
            res.end();
            return;
        } else if (url === '/mouse.png') {
            fs.readFile(getIMG('/mouse.png'), function (err, data) {
            if (err) {
                throw err; // Fail if the file can't be read.
                res.writeHead(200, {"Content-Type": "image/png" });
                res.end(data);
            } 
        });      
        } else if ( url === '/data1') {
            const usia = [];
            for(let i = 0; i < identitas.length; i++) {
                if(identitas[i].age <= 30 && identitas[i].favoriteFruit == 'banana') {
                    usia.push(identitas[i]);
                    console.log(identitas[i].age);
                }};
            res.writeHead(200);
            res.end(JSON.stringify(usia));
        } else if ( url === '/data2') {
            const gender = [];
            for(let i = 0; i < identitas.length; i++) {
                if(identitas[i].gender == 'female' && identitas[i].age >= 30) {
                    gender.push(identitas[i]);
                    console.log(identitas[i].gender);
                }};
            res.writeHead(200);
            res.end(JSON.stringify(gender));
        } else if ( url === '/data3') {
            const warnaMata = [];
            for(let i = 0; i < identitas.length; i++) {
                if(identitas[i].eyeColor == 'blue' && identitas[i].age >= 35 | 40 && identitas[i].favoriteFruit == 'apple') {
                    warnaMata.push(identitas[i]);
                    console.log(identitas[i].eyeColor);
                }};
            const err = {message : "data tidak ditemukan"} 
            const pengecekan= (!warnaMata.length) ? err : warnaMata
            res.writeHead(200);
            res.end(JSON.stringify(pengecekan));
        } else if ( url === '/data4') {
            const perusahaan = [];
            for(let i = 0; i < identitas.length; i++) {
                if(identitas[i].company == 'Pelangi' || identitas[i].company == 'Intel') {
                    perusahaan.push(identitas[i]);
                    console.log(identitas[i].company);
                }};
            res.writeHead(200);
            res.end(JSON.stringify(perusahaan));
        } else if ( url === '/data5') {
            const terdaftar = [];
            for(let i = 0; i < identitas.length; i++) {
                const splite = identitas[i].registered.split("-");
                if(splite[0] <= 2016 && identitas[i].isActive == true) {
                    terdaftar.push(identitas[i]);
                    console.log(identitas[i].registered);
                }};
            res.writeHead(200);
            res.end(JSON.stringify(terdaftar));
        } else {
            // res.write('Hello World');
            fs.readFile('./public/index.html', (error, data) => {
                if(error) {
                    res.writeHead(404);
                    res.write('Error: File not found');
                } else {
                    res.write(data);
                }
                res.end();
            })
        }
    })
    
.listen(3000, () => {
    console.log('Server is listening on port 3000...')
});